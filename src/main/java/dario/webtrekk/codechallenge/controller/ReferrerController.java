package dario.webtrekk.codechallenge.controller;

import dario.webtrekk.codechallenge.exception.GlobalControllerExceptionHandler;
import dario.webtrekk.codechallenge.model.AggregatedReferrer;
import dario.webtrekk.codechallenge.service.ReferrerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/referrers")
public class ReferrerController {
    @Autowired
    private ReferrerService referrerService;

    @ApiOperation(value = "Aggregated Referrer data", notes = "Returns an aggregation of all the stored session matching the optional date and regex filter, aggregated on the dates with daily precision and on the second level on the referrer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of aggregated data"),
            @ApiResponse(code = 400, message = "The client passed a wrongly formatted date (format: yyyy-MM-dd)", response = GlobalControllerExceptionHandler.ErrorDto.class),
            @ApiResponse(code = 500, message = "Internal server error", response = GlobalControllerExceptionHandler.ErrorDto.class)}
    )
    @RequestMapping(method= RequestMethod.GET)
    public ResponseEntity<AggregatedReferrer> getReferrerWithFilters(@RequestParam(value = "from", required=false) String from, @RequestParam(value = "to", required=false) String to, @RequestParam(value = "regex", required=false) String regex) {
        return ResponseEntity.ok(referrerService.getAggregatedReferrer(from, to, regex));
    }
}

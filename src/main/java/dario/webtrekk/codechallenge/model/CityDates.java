package dario.webtrekk.codechallenge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CityDates  implements Comparable<CityDates> {
    private String date;
    private List<City> cities;

    public int compareTo(CityDates other) {
        return this.date.compareTo(other.getDate());
    }
}
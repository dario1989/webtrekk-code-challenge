package dario.webtrekk.codechallenge.service;

import com.jayway.restassured.RestAssured;
import dario.webtrekk.CodechallengeApplication;
import dario.webtrekk.codechallenge.model.AggregatedCities;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(CodechallengeApplication.class)
@IntegrationTest("server.port:0")
public class CitiesControllerIntegrationTest {

    @Value("${local.server.port}")
    int port;
    @Before
    public void init() {
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @Test
    public void testSuccessfullCall() {
        AggregatedCities cities = given().header("Accept", "application/json").get("/cities").then().contentType("application/json").extract().response().then().statusCode(200).contentType("application/json").extract().response().as(AggregatedCities.class);
        Assert.assertNotNull(cities.getDates());
        Assert.assertTrue(cities.getDates().size() != 0);
        Assert.assertNotNull(cities.getDates().iterator().next().getCities().get(0));
    }

    @Test
    public void testSuccessfullCallWithParams() {
        AggregatedCities cities = given().header("Accept", "application/json").get("/cities?regex=Berl.*&from=2012-01-01&to=2015-01-01").then().statusCode(200).contentType("application/json").extract().response().as(AggregatedCities.class);
        Assert.assertNotNull(cities.getDates());
        Assert.assertTrue(cities.getDates().size() != 0);
        Assert.assertNotNull(cities.getDates().iterator().next().getCities().get(0));
    }

    @Test
    public void testEmptyResult() {
        AggregatedCities cities = given().header("Accept", "application/json").get("/cities?regex=Berl.*&from=1931-01-01&to=1931-01-01").then().statusCode(200).contentType("application/json").extract().response().as(AggregatedCities.class);
        Assert.assertTrue(cities.getDates().isEmpty());
    }

    @Test
    public void test400WithBadParams() {
        given().header("Accept", "application/json").get("/cities?regex=Berl.*&from=2012-99-01&to=2015-01-01").then().statusCode(HttpStatus.BAD_REQUEST.value());
    }
}

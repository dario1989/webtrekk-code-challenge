package dario.webtrekk.codechallenge.service;

import com.jayway.restassured.RestAssured;
import dario.webtrekk.CodechallengeApplication;
import dario.webtrekk.codechallenge.model.AggregatedCities;
import dario.webtrekk.codechallenge.model.AggregatedReferrer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(CodechallengeApplication.class)
@IntegrationTest("server.port:0")
public class ReferrerControllerIntegrationTest {

    @Value("${local.server.port}")
    int port;
    @Before
    public void init() {
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @Test
    public void testSuccessfullCall() {
        AggregatedReferrer referrers = given().header("Accept", "application/json").get("/referrers").then().statusCode(200).contentType("application/json").extract().response().as(AggregatedReferrer.class);
        Assert.assertNotNull(referrers.getDates());
        Assert.assertTrue(referrers.getDates().size() != 0);
        Assert.assertNotNull(referrers.getDates().iterator().next().getReferrers().get(0));
    }

    @Test
    public void testSuccessfullCallWithParams() {
        AggregatedReferrer referrers = given().header("Accept", "application/json").get("/referrers?regex=www.googl.*&from=2012-01-01&to=2015-01-01").then().statusCode(200).contentType("application/json").extract().response().as(AggregatedReferrer.class);
        Assert.assertNotNull(referrers.getDates());
        Assert.assertTrue(referrers.getDates().size() != 0);
        Assert.assertNotNull(referrers.getDates().iterator().next().getReferrers().get(0));
    }

    @Test
    public void testEmptyResult() {
        AggregatedReferrer referrers = given().header("Accept", "application/json").get("/referrers?regex=www.googl.*&from=1931-01-01&to=1931-01-01").then().statusCode(200).contentType("application/json").extract().response().as(AggregatedReferrer.class);
        Assert.assertTrue(referrers.getDates().isEmpty());
    }

    @Test
    public void test400WithBadParams() {
        given().header("Accept", "application/json").get("/referrers?regex=www.googl.*&from=2012-99-01&to=2015-01-01").then().statusCode(HttpStatus.BAD_REQUEST.value());
    }
}

package dario.webtrekk.codechallenge.service;

import dario.webtrekk.codechallenge.model.CityDates;
import dario.webtrekk.codechallenge.model.WebtrekkSession;
import dario.webtrekk.codechallenge.utils.DateUtil;
import dario.webtrekk.codechallenge.utils.FilterUtil;
import org.joda.time.DateTime;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class FilteringAndMappingService<T> {

    private Collection<WebtrekkSession> sessions;
    private DateTime from;
    private DateTime to;
    private String regex;
    private Function<Map.Entry<DateTime, Map<String, Long>>, T> aggregator;
    private Function<WebtrekkSession, String> mapper;

    public FilteringAndMappingService<T> setSessions(Collection<WebtrekkSession> sessions) {
        this.sessions = sessions;
        return this;
    }

    public FilteringAndMappingService<T> setFromString(String from) {
        this.from = from != null ? DateUtil.parse(from) : null;
        return this;
    }

    public FilteringAndMappingService<T> setToString(String to) {
        this.to = to != null ? DateUtil.parse(to) : null;
        return this;
    }

    public FilteringAndMappingService<T> setAggregator(Function<Map.Entry<DateTime, Map<String, Long>>, T> aggregator) {
        this.aggregator = aggregator;
        return this;
    }

    public FilteringAndMappingService<T> setMapper(Function<WebtrekkSession, String> mapper) {
        this.mapper = mapper;
        return this;
    }

    public FilteringAndMappingService<T> setRegex(String regex) {
        this.regex = regex;
        return this;
    }

    /**
     * Executes filtering, mapping and aggregation on the whole dataset of sessions.
     * @return
     */
    public Set<T> execute() {
        if(this.from != null && this.to != null && this.from.isAfter(this.to)) {
            throw new RuntimeException("The from date parameter cannot be after the to date parameter!");
        }
        return sessions.stream()
                .filter(filterByDatesAndRegex(from, to, regex, mapper))
                .collect(mapper(mapper))
                .entrySet()
                .stream()
                .map(aggregator)
                .collect(Collectors.toCollection(() -> new TreeSet<T>()));
    }

    /**
     * Given from, to, regex and a mapping function, creates the predicate to filter all Webtrekk sessions within the specified timespan,
     * and of which the field returned by the mapping function matches the passed regular expression
     * @param dateFrom
     * @param dateTo
     * @param cityRegex Regular expression to be applied on the field returned by the mapping function
     * @param mapper The mapping function applied on every Webtrekk session: on the field returned by the function, will be applied the regex.
     * @return
     */

    private Predicate<WebtrekkSession> filterByDatesAndRegex(DateTime dateFrom, DateTime dateTo, String cityRegex, Function<WebtrekkSession, String> mapper) {
        return session -> FilterUtil.filterByDateFrom(session, dateFrom) && FilterUtil.filterByDateTo(session, dateTo) && regexCondition(session, cityRegex, mapper);
    }

    private Collector<WebtrekkSession, ?, Map<DateTime, Map<String, Long>>> mapper(Function<WebtrekkSession, String> mapper) {
        return Collectors.groupingBy(WebtrekkSession::getDate, Collectors.groupingBy(mapper, Collectors.counting()));
    }

    private Boolean regexCondition(WebtrekkSession session, String regex, Function<WebtrekkSession, String> mapper) {
        return Optional.ofNullable(regex)
                .map(r -> mapper.apply(session).matches(r))
                .orElse(Boolean.TRUE);
    }

}

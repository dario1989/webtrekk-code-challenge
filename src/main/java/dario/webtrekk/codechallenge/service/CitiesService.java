package dario.webtrekk.codechallenge.service;

import dario.webtrekk.codechallenge.model.AggregatedCities;
import dario.webtrekk.codechallenge.model.City;
import dario.webtrekk.codechallenge.model.CityDates;
import dario.webtrekk.codechallenge.model.WebtrekkSession;
import dario.webtrekk.codechallenge.repository.SessionRepository;
import dario.webtrekk.codechallenge.utils.DateUtil;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CitiesService {
    private static final Logger log = LoggerFactory.getLogger(CitiesService.class);

    @Autowired
    private SessionRepository sessionRepo;

    /**
     * Given dates from and to, and regular expression regex, returns all the sessions in the specified timespan, with the city field which matches the regex.
     * Then, the data are aggregated on the date (with daily precision) and within each day on the city, counting then all occurrences with the same city
     * @param dateFrom
     * @param dateTo
     * @param citiRegex
     * @return
     */
    public AggregatedCities getAggregatedCities(String dateFrom, String dateTo, String citiRegex) {

        Set<CityDates> filteredSessions = new FilteringAndMappingService<CityDates>()
                .setSessions(sessionRepo.findAll())
                .setAggregator(cityAggregation())
                .setMapper(WebtrekkSession::getCity)
                .setFromString(dateFrom)
                .setToString(dateTo)
                .setRegex(citiRegex)
                .execute();

        return AggregatedCities.builder()
                .dates(filteredSessions)
                .build();
    }

    private Function<Map.Entry<DateTime, Map<String, Long>>, CityDates> cityAggregation() {
        return entry -> CityDates.builder()
                .date(entry.getKey().toString(DateUtil.FORMATTER))
                .cities(entry.getValue()
                        .entrySet()
                        .stream()
                        .map(this::toCityEntry)
                        .collect(Collectors.toList()))
                .build();
    }

    private City toCityEntry(Map.Entry<String, Long> entry) {
        return City.builder()
                .city(entry.getKey())
                .count(entry.getValue().intValue())
                .build();
    }
}

package dario.webtrekk.codechallenge.model;


import dario.webtrekk.codechallenge.utils.DateUtil;
import lombok.Builder;
import lombok.Data;
import org.joda.time.DateTime;

@Data
@Builder
public class WebtrekkSession implements Comparable<WebtrekkSession> {
    private static final String SEPARATOR = ";";
    private DateTime date;
    private String city;
    private String id;
    private String referrer;

    public static WebtrekkSession fromTextLine(String line) {
        String[] split = line.split(SEPARATOR);

        return WebtrekkSession.builder()
                .city(split[8])
                .date(DateUtil.parse(split[3]))
                .id(split[2])
                .referrer(split[19])
                .build();
    }

    @Override
    public int compareTo(WebtrekkSession o) {
        if(this.id.equals(o.getId())) {
            return 0;
        }
        int dateComparison = this.date.compareTo(o.getDate());
        return dateComparison == 0 ? 1 : dateComparison;
    }
}

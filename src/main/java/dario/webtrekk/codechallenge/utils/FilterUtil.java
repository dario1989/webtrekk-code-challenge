package dario.webtrekk.codechallenge.utils;

import dario.webtrekk.codechallenge.model.WebtrekkSession;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.Optional;

public class FilterUtil {
    public static Boolean filterByDateTo(WebtrekkSession session, DateTime to) {
        return Optional.ofNullable(to)
                .map(instant -> !session.getDate().isAfter(instant))
                .orElse(Boolean.TRUE);
    }

    public static Boolean filterByDateFrom(WebtrekkSession session, DateTime from) {
        return Optional.ofNullable(from)
                .map(instant -> !session.getDate().isBefore(instant))
                .orElse(Boolean.TRUE);
    }
}

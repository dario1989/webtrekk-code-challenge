package dario.webtrekk.codechallenge.service;

import dario.webtrekk.codechallenge.model.*;
import dario.webtrekk.codechallenge.repository.SessionRepository;
import dario.webtrekk.codechallenge.utils.DateUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

@RunWith(MockitoJUnitRunner.class)
public class CitiesServiceTest {

    @Mock
    private SessionRepository sessionRepo;
    @InjectMocks
    private CitiesService citiesService;

    @Test
    public void getAggregatedCities() throws Exception {
        Mockito.when(sessionRepo.findAll()).thenReturn(dummySessions());
        AggregatedCities result2016 = citiesService.getAggregatedCities("2016-01-01","2016-01-03", ".*");
        Assert.assertEquals(result2016.getDates().size(), 1);
        Assert.assertEquals(result2016.getDates().iterator().next().getCities().get(0).getCity(), "Rom");

        AggregatedCities resultAllYears = citiesService.getAggregatedCities("1970-01-01","2999-01-01", ".*");
        Assert.assertTrue(resultAllYears.getDates().stream().map(c -> c.getCities().stream().map(City::getCount).reduce(Integer::sum).get()).reduce(Integer::sum).get()==sessionRepo.findAll().size());

        AggregatedCities resultBerlin = citiesService.getAggregatedCities("1970-01-01","2999-01-01", "Berlin");
        Assert.assertEquals(resultBerlin.getDates().iterator().next().getDate(), "2012-01-01");
        Assert.assertEquals(resultBerlin.getDates().iterator().next().getCities().get(0).getCount(), 2);

        AggregatedCities resultBerlinMatch = citiesService.getAggregatedCities("1970-01-01","2016-01-01", ".*Berlin.*");
        Assert.assertTrue(resultBerlinMatch.getDates().stream().map(c -> c.getCities().stream().map(City::getCount).reduce(Integer::sum).get()).reduce(Integer::sum).get()== 5);

        AggregatedCities resultBerlin2012 = citiesService.getAggregatedCities("2012-01-01","2012-03-01", "Berlin");
        for(CityDates referrerDates : resultBerlin2012.getDates()) {
            switch (referrerDates.getDate()) {
                case "2012-01-01":
                    Assert.assertTrue(referrerDates.getCities().size() == 1);
                    Assert.assertTrue(referrerDates.getCities().get(0).getCount() == 2);
                    break;
                case "2012-02-01":
                    Assert.assertTrue(referrerDates.getCities().size() == 1);
                    Assert.assertTrue(referrerDates.getCities().get(0).getCount() == 1);
                    break;
                case "2012-03-01":
                    Assert.assertTrue(referrerDates.getCities().size() == 1);
                    Assert.assertTrue(referrerDates.getCities().get(0).getCount() == 1);
                    break;

            }
        }
    }

    @Test(expected = RuntimeException.class)
    public void getAggregatedCitiesBadDateInFrom() throws Exception {
        Mockito.when(sessionRepo.findAll()).thenReturn(dummySessions());
        citiesService.getAggregatedCities("2016-01-99","2016-01-03", ".*");
    }

    @Test(expected = RuntimeException.class)
    public void getAggregatedCitiesBadDateInTo() throws Exception {
        Mockito.when(sessionRepo.findAll()).thenReturn(dummySessions());
        citiesService.getAggregatedCities("2016-01-01","2016-99-03", ".*");
    }

    private Collection<WebtrekkSession> dummySessions() {
        Set<WebtrekkSession> sessions = new TreeSet<>();
        sessions.add(WebtrekkSession.builder()
                .id("1")
                .city("Berlin")
                .date(DateUtil.parse("2012-01-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .city("Berlin")
                .id("2")
                .date(DateUtil.parse("2012-01-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .city("Berlin")
                .id("10")
                .date(DateUtil.parse("2012-02-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .city("Berlino")
                .id("3")
                .date(DateUtil.parse("2012-01-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .city("prefixBerlino")
                .id("4")
                .date(DateUtil.parse("2013-01-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .city("Munich")
                .id("5")
                .date(DateUtil.parse("2014-02-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .city("Munich")
                .id("6")
                .date(DateUtil.parse("2015-03-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .city("Rom")
                .id("7")
                .date(DateUtil.parse("2016-01-02"))
                .build());
        return sessions;
    }
}
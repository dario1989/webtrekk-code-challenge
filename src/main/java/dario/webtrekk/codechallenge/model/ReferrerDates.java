package dario.webtrekk.codechallenge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReferrerDates implements Comparable<ReferrerDates> {
    private String date;
    private List<Referrer> referrers;

    public int compareTo(ReferrerDates other) {
        return this.date.compareTo(other.getDate());
    }
}

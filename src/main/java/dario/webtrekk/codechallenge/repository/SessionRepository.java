package dario.webtrekk.codechallenge.repository;

import dario.webtrekk.codechallenge.model.WebtrekkSession;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class SessionRepository {

    private static final Logger log = LoggerFactory.getLogger(SessionRepository.class);

    private Collection<WebtrekkSession> sessions;

    @PostConstruct
    private void init() {
        log.info("Starting load of Sessions file...");
        try(InputStream fileInputStream = new ClassPathResource("sessions.csv").getInputStream()) {
            List<String> fileLines = IOUtils.readLines(fileInputStream);
            sessions = fileLines.parallelStream().map(WebtrekkSession::fromTextLine).collect(Collectors.toSet());
            log.info("Load of Sessions file completed, loaded {} sessions", sessions.size());
        } catch(IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public Collection<WebtrekkSession> findAll() {
        return this.sessions;
    }
}

package dario.webtrekk.codechallenge.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtil {
    public static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd");

    public static DateTime parse(String date) {
        try {
            return DateTime.parse(date, FORMATTER);
        } catch(Exception e) {
            throw new RuntimeException("The value " + date + " is not a valid date!");
        }
    }
}

package dario.webtrekk.codechallenge.service;

import dario.webtrekk.codechallenge.model.*;
import dario.webtrekk.codechallenge.repository.SessionRepository;
import dario.webtrekk.codechallenge.utils.DateUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

@RunWith(MockitoJUnitRunner.class)
public class ReferrerServiceTest {

    @Mock
    private SessionRepository sessionRepo;
    @InjectMocks
    private ReferrerService referrerService;

    @Test
    public void getAggregatedReferrers() throws Exception {
        Mockito.when(sessionRepo.findAll()).thenReturn(dummySessions());
        AggregatedReferrer result2016 = referrerService.getAggregatedReferrer("2016-01-01","2016-01-03", ".*");
        Assert.assertEquals(result2016.getDates().size(), 1);
        Assert.assertEquals(result2016.getDates().iterator().next().getReferrers().get(0).getReferrer(), "www.google.it");

        AggregatedReferrer resultAllYears = referrerService.getAggregatedReferrer("1970-01-01","2999-01-01", ".*");
        Assert.assertTrue(resultAllYears.getDates().stream().map(c -> c.getReferrers().stream().map(Referrer::getCount).reduce(Integer::sum).get()).reduce(Integer::sum).get()==sessionRepo.findAll().size());

        AggregatedReferrer resultBerlin = referrerService.getAggregatedReferrer("1970-01-01","2999-01-01", "www.smtg.com");
        Assert.assertEquals(resultBerlin.getDates().iterator().next().getDate(), "2012-01-01");
        Assert.assertEquals(resultBerlin.getDates().iterator().next().getReferrers().get(0).getCount(), 2);

        AggregatedReferrer resultBerlinMatch = referrerService.getAggregatedReferrer("1970-01-01","2016-01-01", ".*www.smtg.com.*");
        Assert.assertTrue(resultBerlinMatch.getDates().stream().map(c -> c.getReferrers().stream().map(Referrer::getCount).reduce(Integer::sum).get()).reduce(Integer::sum).get()== 5);

        AggregatedReferrer resultBerlin2012 = referrerService.getAggregatedReferrer("2012-01-01","2012-03-01", "www.smtg.com");
        for(ReferrerDates referrerDates : resultBerlin2012.getDates()) {
            switch (referrerDates.getDate()) {
                case "2012-01-01": Assert.assertTrue(referrerDates.getReferrers().size() == 1);
                    Assert.assertTrue(referrerDates.getReferrers().get(0).getCount() == 2);
                    break;
                case "2012-02-01": Assert.assertTrue(referrerDates.getReferrers().size() == 1);
                    Assert.assertTrue(referrerDates.getReferrers().get(0).getCount() == 1);
                    break;
                case "2012-03-01": Assert.assertTrue(referrerDates.getReferrers().size() == 1);
                    Assert.assertTrue(referrerDates.getReferrers().get(0).getCount() == 1);
                    break;

            }
        }
    }

    @Test(expected = RuntimeException.class)
    public void getAggregatedReferrerBadDateInFrom() throws Exception {
        Mockito.when(sessionRepo.findAll()).thenReturn(dummySessions());
        referrerService.getAggregatedReferrer("2016-01-99","2016-01-03", ".*");
    }

    @Test(expected = RuntimeException.class)
    public void getAggregatedReferresBadDateInTo() throws Exception {
        Mockito.when(sessionRepo.findAll()).thenReturn(dummySessions());
        referrerService.getAggregatedReferrer("2016-01-01","2016-99-03", ".*");
    }

    private Collection<WebtrekkSession> dummySessions() {
        Set<WebtrekkSession> sessions = new TreeSet<>();
        sessions.add(WebtrekkSession.builder()
                .id("1")
                .referrer("www.smtg.com")
                .date(DateUtil.parse("2012-01-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .referrer("www.smtg.com")
                .id("2")
                .date(DateUtil.parse("2012-01-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .referrer("www.smtg.com")
                .id("10")
                .date(DateUtil.parse("2012-02-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .referrer("www.smtg.comsuffix")
                .id("3")
                .date(DateUtil.parse("2012-01-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .referrer("prefixwww.smtg.com")
                .id("4")
                .date(DateUtil.parse("2013-01-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .referrer("www.test.com")
                .id("5")
                .date(DateUtil.parse("2014-02-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .referrer("www.test.com")
                .id("6")
                .date(DateUtil.parse("2015-03-01"))
                .build());
        sessions.add(WebtrekkSession.builder()
                .referrer("www.google.it")
                .id("7")
                .date(DateUtil.parse("2016-01-02"))
                .build());
        return sessions;
    }
}
package dario.webtrekk.codechallenge.service;

import dario.webtrekk.codechallenge.model.*;
import dario.webtrekk.codechallenge.repository.SessionRepository;
import dario.webtrekk.codechallenge.utils.DateUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ReferrerService {

    @Autowired
    private SessionRepository sessionRepo;

    /**
     * Given dates from and to, and regular expression regex, returns all the sessions in the specified timespan, with the referrer field which matches the regex.
     * Then, the data are aggregated on the date (with daily precision) and within each day on the referrer, counting then all occurrences with the same referrer
     * @param dateFrom
     * @param dateTo
     * @param referrerRegex
     * @return
     */
    public AggregatedReferrer getAggregatedReferrer(String dateFrom, String dateTo, String referrerRegex) {

        Set<ReferrerDates> filteredSessions = new FilteringAndMappingService<ReferrerDates>()
                .setSessions(sessionRepo.findAll())
                .setAggregator(referrerAggregation())
                .setMapper(WebtrekkSession::getReferrer)
                .setFromString(dateFrom)
                .setToString(dateTo)
                .setRegex(referrerRegex)
                .execute();

        return AggregatedReferrer.builder()
                .dates(filteredSessions)
                .build();
    }

    private Function<Map.Entry<DateTime, Map<String, Long>>, ReferrerDates> referrerAggregation() {
        return entry -> ReferrerDates.builder()
                .date(entry.getKey().toString(DateUtil.FORMATTER))
                .referrers(entry.getValue()
                        .entrySet()
                        .stream()
                        .map(this::toReferrerEntry)
                        .collect(Collectors.toList()))
                .build();
    }

    private Referrer toReferrerEntry(Map.Entry<String, Long> entry) {
        return Referrer.builder()
                .referrer(entry.getKey())
                .count(entry.getValue().intValue())
                .build();
    }
}

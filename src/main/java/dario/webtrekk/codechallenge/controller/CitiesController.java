package dario.webtrekk.codechallenge.controller;

import dario.webtrekk.codechallenge.exception.GlobalControllerExceptionHandler;
import dario.webtrekk.codechallenge.model.AggregatedCities;
import dario.webtrekk.codechallenge.service.CitiesService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/cities", produces = "application/json")
public class CitiesController {
    @Autowired
    private CitiesService citiesService;

    @ApiOperation(value = "Aggregated Cities data", notes = "Returns an aggregation of all the stored session matching the optional date and regex filter, aggregated on the dates with daily precision and on the second level on the city name")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of aggregated data"),
            @ApiResponse(code = 400, message = "The client passed a wrongly formatted date (format: yyyy-MM-dd)", response = GlobalControllerExceptionHandler.ErrorDto.class),
            @ApiResponse(code = 500, message = "Internal server error", response = GlobalControllerExceptionHandler.ErrorDto.class)}
    )
    @RequestMapping(method= RequestMethod.GET, produces = "application/json")
    public ResponseEntity<AggregatedCities> getCitiesWithFilters(@RequestParam(value = "from", required=false) String from, @RequestParam(value = "to", required=false) String to, @RequestParam(value = "regex", required=false) String regex) {
        return ResponseEntity.ok(citiesService.getAggregatedCities(from, to, regex));
    }
}

package dario.webtrekk.codechallenge.exception;

import lombok.Builder;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500, generic exception
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ErrorDto handleGenericException(Exception e) {
        log.error("StackTrace", e);
        return ErrorDto.builder().message("Oops! An error occurred!: " +  e.getMessage()).build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)  // 400
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ErrorDto handleRunTimeException(RuntimeException e) {
        log.error("StackTrace", e);
        return ErrorDto.builder().message(e.getMessage()).build();
    }

    @Data
    @Builder
    public static class ErrorDto {
        private String message;
    }
}